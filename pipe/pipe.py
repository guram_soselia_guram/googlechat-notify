from bitbucket_pipes_toolkit import Pipe, yaml, get_variable
from json import dumps

from httplib2 import Http
from bitbucket_pipes_toolkit import Pipe, get_logger

logger = get_logger()
schema = {
    "WEBHOOK_URL": {
        "type": "string",
        "required": True
    },
    "TITLE": {
        "type": "string",
        "required": True
    },
    "IMAGE_URL": {
        "type": "string",
        "required": True
    },
    "BUILD_NUMBER": {
        "type": "string",
        "required": True
    },
    "BRANCH_NAME": {
        "type": "string",
        "required": True
    },
    "STATUS": {
        "type": "string",
        "required": True
    },
    "PIPELINE_URL": {
        "type": "string",
        "required": True
    },
    "PULL_REQUEST_URL": {
        "type": "string",
        "required": True
    },
    "DEBUG": {
        "type": "boolean",
        "default": False
    }
}


class DemoPipe(Pipe):
    def run(self):
        super().run()

        webhook_url = self.get_variable("WEBHOOK_URL")
        title = self.get_variable("TITLE")
        imageUrl = self.get_variable("IMAGE_URL")
        buildNumber = self.get_variable("BUILD_NUMBER")
        branchName = self.get_variable("BRANCH_NAME")
        status = self.get_variable("STATUS")
        pipelineUrl = self.get_variable("PIPELINE_URL")
        pipelineRequestUrl = self.get_variable("PULL_REQUEST_URL")

        bot_message = {
            "cards": [
                {
                    "header": {
                        "title": title,
                        "subtitle": "Build #"+buildNumber,
                        "imageUrl": imageUrl
                    },
                    "sections": [
                        {
                            "widgets": [
                                {
                                    "keyValue": {
                                        "topLabel": "Branch",
                                        "content": "🔀 " + branchName
                                    }
                                },
                                {
                                    "keyValue": {
                                        "topLabel": "Status",
                                        "content": status
                                    }
                                }
                            ]
                        },
                        {
                            "widgets": [
                                {
                                    "buttons": [
                                        {
                                            "textButton": {
                                                "text": "OPEN PIPELINE",
                                                "onClick": {
                                                        "openLink": {
                                                            "url": pipelineUrl + "#!/results/" + buildNumber
                                                        }
                                                }
                                            }
                                        },
                                        {
                                            "textButton": {
                                                "text": "OPEN PULL REQUEST",
                                                "onClick": {
                                                        "openLink": {
                                                            "url": pipelineRequestUrl
                                                        }
                                                }
                                            }
                                        },
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        }
        message_headers = {'Content-Type': 'application/json; charset=UTF-8'}

        http_obj = Http()

        response = http_obj.request(
            uri=webhook_url,
            method='POST',
            headers=message_headers,
            body=dumps(bot_message),
        )
        print(response)

        self.success(message='success!')


if __name__ == '__main__':
    pipe = DemoPipe(pipe_metadata='/pipe.yml', schema=schema)
    pipe.run()
